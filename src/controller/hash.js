const crypto = require('crypto');

function getHash(pass) {
    return crypto.createHash('sha256').update(pass).digest('hex');
}

module.exports = { getHash };
