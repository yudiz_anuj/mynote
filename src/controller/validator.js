const { check, body, validationResult } = require('express-validator');
function exp_validate() {
  return [
    // prettier-ignore
    check('sName').trim().not().isEmpty().withMessage('Not valid Name'),
    // prettier-ignore
    check('sEmail').isEmail().withMessage('Not valid Email'),
    // prettier-ignore
    check('sPassword').not().isEmpty().isLength({min:8,max:20}).withMessage('Not valid Password'),
    // prettier-ignore
    check('nMobile').isLength({min:8,max:20}).withMessage('Invalid Mobile'),
  ];
}
module.exports = { exp_validate };
