const { getUsers, getActiveId, getActiveUser } = require('./utility');
const fs = require('fs');

module.exports = {
  get: (req, res) => {
    const oUsers = getUsers();
    const sActiveId = getActiveId();
    const oActiveUser = oUsers[sActiveId];

    if (oActiveUser) {
      const oMyNotes = oActiveUser.oNotesList;
      res.status(200).send(JSON.stringify(oMyNotes));
    } else {
      res.status(500).send('please login to access your todo');
    }
  },
  post: (req, res) => {
    const oUsers = getUsers();
    const sActiveId = getActiveId();
    const oActiveUser = oUsers[sActiveId];
    if (oActiveUser) {
      const oMyNotes = oActiveUser.oNotesList;

      const sId = `o${Object.keys(oMyNotes).length + 1}`;
      const sTitle = req.body.sTitle;
      const sBody = req.body.sBody;

      const data = {
        sTitle: sTitle,
        sBody: sBody,
      };

      const oNotesList = {};
      for (let note in oMyNotes) {
        oNotesList[note] = oMyNotes[note];
      }
      oNotesList[sId] = data;

      oActiveUser.oNotesList = oNotesList;
      oUsers.sActiveId = oActiveUser;

      fs.writeFileSync(
        `${__dirname}/../model/userList.json`,
        JSON.stringify(oUsers)
      );

      res.status(200).send(JSON.stringify(oMyNotes));
    } else {
      res.status(500).send('please login to access your notes');
    }
  },
  delete: (req, res) => {
    const oUsers = getUsers();
    const sActiveId = getActiveId();
    const oActiveUser = oUsers[sActiveId];
    if (oActiveUser) {
      const sId = req.body.sId;
      const oMyNotes = oActiveUser.oNotesList;
      delete oMyNotes[sId];
      oUsers[sActiveId].oNotesList = oMyNotes;
      fs.writeFileSync(
        `${__dirname}/../model/userList.json`,
        JSON.stringify(oUsers)
      );
      res.status(200).send('deleted successfully');
    } else {
      res.status(500).send('please login to access your notes');
    }
  },
};
