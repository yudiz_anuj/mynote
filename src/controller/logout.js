const fs = require('fs');

module.exports = {
    get: (req, res) => {
        fs.writeFileSync(`${__dirname}/../model/activeUser.json`, '{}');
        res.status(200).send('logged out');
    },
};
