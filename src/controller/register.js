const fs = require('fs');
const { check, body, validationResult } = require('express-validator');

const { getUsers } = require('./utility');
const { getHash } = require('./hash');
const { exp_validate } = require('./validator');

module.exports = {
  post: (req, res) => {
    const oUsers = getUsers();

    const sId = `yudiz_${Object.keys(oUsers).length + 1}`,
      sName = req.body.sName,
      nMobile = req.body.nMobile,
      sEmail = req.body.sEmail,
      sPassword = getHash(req.body.sPassword),
      oNotesList = req.body.oNotesList;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.log(errors.mapped());
      return res.json(errors);
    } else {
      let isUnique = true;

      for (let usr in oUsers) {
        let data = oUsers[usr];
        if (sEmail === data.sEmail) {
          isUnique = false;
        }
      }
      if (!isUnique) {
        res.status(500).send('User already exists');
      } else {
        let data = {
          sName: sName,
          nMobile: nMobile,
          sEmail: sEmail,
          sPassword: sPassword,
          oNotesList: oNotesList,
        };
        // oNewUser = JSON.stringify(data);
        const oUserList = {};
        for (let u in oUsers) {
          oUserList[u] = oUsers[u];
        }
        oUserList[sId] = data;
        fs.writeFileSync(
          `${__dirname}/../model/userList.json`,
          JSON.stringify(oUserList)
        );
        res.status(500).send('user registered successfully');
      }
    }
  },
};
