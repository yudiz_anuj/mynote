const fs = require('fs');
const { getUsers } = require('./utility');
const { getHash } = require('../controller/hash');

module.exports = {
    get: (req, res) => {
        const sId = req.body.sId;
        const sPassword = getHash(req.body.sPassword);

        const oUsers = getUsers();
        const oUser = oUsers[sId];

        if (oUser) {
            if (oUser.sPassword == sPassword) {
                const sActiveId = JSON.stringify({ sActiveId: sId });
                fs.writeFileSync(
                    `${__dirname}/../model/activeUser.json`,
                    sActiveId
                );
                res.status(200).send(JSON.stringify(oUser));
            } else res.status(500).send('Invalid Password');
        } else {
            res.status(500).send('Invalid  Id');
        }
    },
};
