const fs = require('fs');

const getUsers = () => {
    const oUsers = fs.readFileSync(
        `${__dirname}/../model/userList.json`,
        'utf-8'
    );
    return JSON.parse(oUsers);
};

const getActiveId = () => {
    let sActiveId = fs.readFileSync(
        `${__dirname}/../model/activeUser.json`,
        'utf-8'
    );
    sActiveId = JSON.parse(sActiveId);
    return sActiveId['sActiveId'];
};

const getActiveUser = () => {
    const oUsers = getUsers(),
        sActiveId = getActiveId();

    const oActiveUser = oUsers[sActiveId];
    return oActiveUser;
};

module.exports = { getUsers, getActiveId, getActiveUser };
