"use strict";
const express = require("express");
let router = express.Router();

const logoutController = require("../controller/logout");

router.get("/", logoutController.get);

module.exports = router;
