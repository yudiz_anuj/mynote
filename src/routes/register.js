'use strict';
const express = require('express');
let router = express.Router();

const registerController = require('../controller/register');
const { exp_validate } = require('../controller/validator');

router.post('/', exp_validate(), registerController.post);

module.exports = router;
