'use strict';
const express = require('express');
let router = express.Router();

const notesController = require('../controller/notes');

router
    .get('/', notesController.get)
    .post('/', notesController.post)
    .delete('/', notesController.delete);

module.exports = router;
