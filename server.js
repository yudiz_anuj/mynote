'use strict';
const crypto = require('crypto');
const fs = require('fs');

const express = require('express');
const app = express();

app.use(express.json()); //Used to parse JSON bodies
app.use(express.urlencoded({ extended: true })); //Parse URL-encoded bodies

const mRegister = require('./src/routes/register');
const mLogin = require('./src/routes/login');
const mLogout = require('./src/routes/logout');
const mNotes = require('./src/routes/notes');

app.use('/register', mRegister);
app.use('/login', mLogin);
app.use('/notes', mNotes);
app.use('/logout', mLogout);

const port = process.env.port || 4000;
app.listen(port, (err) => {
  if (err) return console.log('Error', err);
  console.log(`Listening on http://localhost:${port}/`);
});
